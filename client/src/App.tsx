import './App.css';
import Login from './components/login/login';
import {Chat} from './components/chat/chat';

// var client = new WebSocket("ws://localhost:8080/ws");
var client = new WebSocket("ws://3.82.197.174:8080/ws");

client.binaryType = "arraybuffer";

function App() {
  return (
    <div className="App">
      <Chat client={client}/>
    </div>
  );
}

export default App;
