FROM golang:1.19beta1-alpine3.16

RUN mkdir -p /app

WORKDIR /app

# COPY ./client/build ./build/

COPY ./server /app/

RUN go mod download all

RUN cd cmd && go build -o /go-app

CMD [ "/go-app" ]

# COPY waitformysql.sh /waitformysql.sh

# RUN chmod +x /waitformysql.sh

# RUN apk add --no-cache bash

# CMD [ "/waitformysql.sh","mysqldb:3306", "--", "/go-app" ]
