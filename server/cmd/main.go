// Copyright 2013 The Gorilla WebSocket Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package main

import (
	"flag"
	"fmt"
	"log"
	"net/http"
	"os"
	"strings"

	"gitlab.com/vund5/chat-application/server/chat"
)

var addr = flag.String("addr", ":8080", "http service address")

func main() {
	// db_main_host := os.Getenv("DB.host")
	// database, err := ent.Open("mysql", "root:duyvu1109@tcp(mysqldb:3306)/chatapp?parseTime=True")
	// if err != nil {
	// 	log.Fatalf("failed opening connection to mySQL: %v", err)
	// }
	// fmt.Print("Connecting to MySQL in port 3306 !\n")
	// defer database.Close()
	// // Run the auto migration tool.
	// if err := database.Schema.Create(context.Background()); err != nil {
	// 	log.Fatalf("failed creating schema resources: %v", err)
	// }

	hub := chat.NewHub()
	go hub.Run()

	fmt.Print("Created Hub successfully !\n")
	mydir, _ := os.Getwd()
	var dir = mydir + "/build"
	// fmt.Println(dir)
	fileServe := http.FileServer(http.Dir(dir))
	http.Handle("/", http.StripPrefix(strings.TrimRight("/", "/"), fileServe))
	fmt.Println(dir)
	http.HandleFunc("/ws", func(w http.ResponseWriter, r *http.Request) {
		chat.ServeWs(hub, w, r)
	})

	err := http.ListenAndServe(*addr, nil)
	if err != nil {
		log.Fatalf("ListenAndServe: %v", err)
	}
	fmt.Println("Ready")
}
